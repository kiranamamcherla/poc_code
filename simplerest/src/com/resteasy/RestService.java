package com.resteasy;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/getdata")
public class RestService {

	@Path("/sample")
	@GET
	@Produces("application/json; charset=UTF-8")
	public RestData getSample() {
		return new RestData();
	}
}
