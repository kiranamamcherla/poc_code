package com.resteasy;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RestData {

	private long[][] data = new long[5][5];

	public RestData() {
		GetDataDAO getDataDAO = new GetDataDAO();
		List<SampleData> sampledataList = getDataDAO.getSampleData();
		for (SampleData sampleData : sampledataList) {
			for (int i = 0; i < 2; i++) {
				data[i][0] = sampleData.getData1();
				data[i][1] = sampleData.getData2();
			}
		}
	}

	public long[][] getData() {

		return data;
	}

	public void setData(long[][] data) {

		this.data = data;
	}
}
