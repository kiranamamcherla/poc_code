package com.resteasy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GetDataDAO {

	public GetDataDAO() {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public List<SampleData> getSampleData() {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultset = null;
		List<SampleData> sampleDataList = new ArrayList<SampleData>();
		try {

			connection = DriverManager
					.getConnection("jdbc:sqlite:D:/sqllitedb/javapoc.db");
			System.out.println("Opened database successfully");

			statement = connection.createStatement();
			resultset = statement.executeQuery("SELECT * FROM SAMPLEDATA");

			while (resultset.next()) {
				long data1 = resultset.getLong("data1");
				long data2 = resultset.getLong("data2");
				SampleData sampleData = new SampleData();
				sampleData.setData1(data1);
				sampleData.setData2(data2);
				sampleDataList.add(sampleData);
			}

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		} finally {
			try {
				resultset.close();
				statement.close();
				connection.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		System.out.println("Operation done successfully");
		return sampleDataList;

	}

}
